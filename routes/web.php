<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group( ['middleware' => ['referral']], function () {
    Route::get( '/', 'Frontend\MainController@index' )->name( 'home' );

    Route::group( ['middleware' => ['auth']], function () {
        Route::get( '/profile', 'Frontend\ProfileController@index' )->name( 'profile' );
        Route::get( '/profile/business', 'Frontend\ProfileController@business' )->name( 'profile.business' );
        Route::get( '/business/payment', 'Frontend\ProfileController@payment' )->name( 'business.payment' );

    } );

    Route::get( '/profile', 'Frontend\ProfileController@index' )->name( 'profile' );
    Route::get( '/profile/business', 'Frontend\ProfileController@business' )->name( 'profile.business' );
    Route::get( '/business/payment', 'Frontend\ProfileController@payment' )->name( 'business.payment' );
    Route::put( '/profile/{id}', 'Frontend\ProfileController@update' )->name( 'profile.update' );
    Route::put( '/business/update/{id}', 'Frontend\ProfileController@businessupdate' )->name( 'business.update' );

    Route::get( '/page/{slug}', 'Frontend\MainController@show' )->name( 'page' );

    Route::get( '/promotions', 'Frontend\PromotionController@index' )->name( 'promotions.index' );

    Route::get( '/tours', 'Frontend\ToursController@index' )->name( 'tours.index' );
    Route::get( '/tours/{slug}', 'Frontend\ToursController@show' )->name( 'tours.show' );

    Route::get( '/visa', 'Frontend\InsuranceController@visa' )->name( 'visa.index' );
    Route::get( '/visa/{slug}', 'Frontend\InsuranceController@show' )->name( 'visa.show' );

    Route::get( '/insurance', 'Frontend\InsuranceController@index' )->name( 'insurance.index' );
    Route::get( '/insurance/{slug}', 'Frontend\InsuranceController@show' )->name( 'insurance.show' );

    Route::group( ['middleware' => ['auth'], 'namespace' => 'Frontend'], function () {

        Route::group( ['as' => 'tours.'], function () {

            Route::match( ['get'], '/tours/payment/{slug}', [
                'as'   => 'payment',
                'uses' => 'ToursController@payment',
            ] );

            Route::match( ['post'], 'tours/payment/store', [
                'as'   => 'store',
                'uses' => 'OrdersController@store',
            ] );

            Route::match( ['put'], 'tours/payment/{id}/repay', [
                'as'   => 'repay',
                'uses' => 'OrdersController@repay',
            ] );

            Route::match( ['get', 'post'], 'tours/payment/paypal/success', [
                'as'   => 'paypal.success',
                'uses' => 'OrdersController@paypalSuccess',
            ] );

            Route::match( ['get'], 'tours/order/completed', [
                'as'   => 'thank',
                'uses' => 'PageController@thank',
            ] );

            Route::match( ['get'], 'tours/order/fail', [
                'as'   => 'fail',
                'uses' => 'PageController@fail',
            ] );

        } );

        Route::group( ['as' => 'insurance.'], function () {

            Route::match( ['get'], '/insurance/payment/{slug}', [
                'as'   => 'payment',
                'uses' => 'InsuranceController@payment',
            ] );

            Route::match( ['post'], 'insurance/payment/store', [
                'as'   => 'store',
                'uses' => 'InsuranceController@store',
            ] );

        } );

        Route::group( ['as' => 'member.'], function () {

            Route::match( ['get'], '/member/transection', [
                'as'   => 'income',
                'uses' => 'MemberController@income',
            ] );

            Route::match( ['post'], '/member/transection/withdraw', [
                'as'   => 'withdraw',
                'uses' => 'MemberController@withdraw',
            ] );

            Route::match( ['put'], '/member/transection/withdraw/update/{id}', [
                'as'   => 'withdraw.update',
                'uses' => 'MemberController@withdrawUpdate',
            ] );

            Route::match( ['get'], '/member/order', [
                'as'   => 'order',
                'uses' => 'OrdersController@index',
            ] );

            Route::match( ['get'], '/member/order/detail/{id}', [
                'as'   => 'order.detail',
                'uses' => 'OrdersController@show',
            ] );

        } );

    } );

    Route::get( '/cart', 'Frontend\CartController@index' )->name( 'cart.index' );
    Route::get( 'add-to-cart/{id}', 'Frontend\CartController@addToCart' );

    Route::get( '/services', 'Frontend\ServiceController@index' )->name( 'services.index' );
    Route::get( '/services/{slug}', 'Frontend\ServiceController@show' )->name( 'services.show' );

    Route::get( '/articles', 'Frontend\ArticleController@index' )->name( 'articles.index' );
    Route::get( '/articles/{slug}', 'Frontend\ArticleController@show' )->name( 'articles.show' );
    Route::get( '/articles/tag/{slug}', 'Frontend\ArticleController@showTag' )->name( 'articles.tag' );

    Route::get( '/about-us', 'Frontend\AboutusController@index' )->name( 'about-us.index' );

    Route::get( '/contact-us', 'Frontend\ContactController@index' )->name( 'contact-us.index' );
    Route::post( '/contact-us', 'Frontend\ContactController@store' )->name( 'contact-us.store' );

    Route::get( '/facebook/redirect', 'SocialAuthController@redirect' )->name( 'facebook.redirect' );
    Route::get( '/facebook/callback', 'SocialAuthController@callback' );

    Auth::routes();
} );
