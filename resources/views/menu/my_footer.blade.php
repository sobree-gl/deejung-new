<div class="col-md-2 mb-3">
    <a class="" href="{{ url('/') }}">{{ 'หน้าแรก' }}
        <span class="sr-only">(current)</span>
    </a>
</div>
@php
if (Voyager::translatable($items)) {
$items = $items->load('translations');
}
@endphp
@foreach ($items as $menu_item)
@php
$originalItem = $menu_item;
if (Voyager::translatable($menu_item)) {
$menu_item = $menu_item->translate($options->locale);
}
$isActive = null;
$styles = null;
$icon = null;
// Background Color or Color
if (isset($options->color) && $options->color == true) {
$styles = 'color:'.$menu_item->color;
}
if (isset($options->background) && $options->background == true) {
$styles = 'background-color:'.$menu_item->color;
}
// Check if link is current
if(url($menu_item->link()) == url()->current()){
$isActive = 'active';
}
// Set Icon
if(isset($options->icon) && $options->icon == true){
$icon = '<i class="' . $menu_item->icon_class . '"></i>';
}
@endphp
@if(isset($isActive))
<div class="col-md-2 mb-3">
    <a class="{{$isActive}}" href="{{ $menu_item->link() }}">{{ $menu_item->title }}
        <span class="sr-only">(current)</span>
    </a>
</div>
@else
<div class="col-md-2 mb-3">
    <a class="" href="{{ $menu_item->link() }}">{{ $menu_item->title }}
    </a>
</div>
@endif
@if(!$originalItem->children->isEmpty())
@endif
@endforeach
