<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TransectionController as Transection;
use App\User;
use Cookie;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware( 'guest' );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data )
    {
        return Validator::make( $data, [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ] );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create( array $data )
    {
        $referred_by  = Cookie::get( 'referral' );
        $rendom       = Str::random( 10 );
        $affiliate_id = User::where( 'affiliate_id', $rendom )->count();
        if ( $affiliate_id > 0 ) {
            $rendom = Str::random( 10 );
        } else {
            if ( !empty( $referred_by ) ) {
                $affiliate = User::where( 'affiliate_id', $referred_by )->first();
            }
            $user = User::create( [
                'name'         => $data['name'],
                'email'        => $data['email'],
                'password'     => Hash::make( $data['password'] ),
                'affiliate_id' => $rendom,
                'referred_by'  => $referred_by ?? null,
                'role_id'      => 3,
                'avatar'       => $data['avatar'] ?? config( 'voyager.user.default_avatar', 'users/default.png' ),
            ] );

            Transection::store( 'REGISTER', 'COMMISTION', $referred_by ?? '', 5900, '', $user->id );

            return $user;
        }
    }
}
