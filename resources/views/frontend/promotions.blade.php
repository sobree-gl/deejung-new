@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="">

    <!-- our_latest_blog_start -->
    <div class="our_latest_blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-100">
                        <h3>โปรโมชั่น</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @if(sizeof($promotions) > 0)
                @foreach ($promotions as $promotion)
                <div class="col-xl-4 col-md-4">
                    <div class="single_latest_blog">
                        <div class="thumb">
                            <img src="{{ Voyager::image($promotion->thumbnail('medium')) }}" alt="">
                        </div>
                        <div class="content_blog">
                            <div class="date">
                                <p>12 Jun, 2019 in <a href="#">Design tips</a></p>
                            </div>
                            <div class="blog_meta">
                                <h3><a href="#">{{ $promotion->title }}</a></h3>
                            </div>
                            <div class="dot-ellipsis" style="height: 55px;color: rgb(113, 113, 113);">
                                {!! $promotion->detail !!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-xl-12">
                    <div class="alert alert-danger" role="alert">
                        ไม่พบข้อมูล
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- our_latest_blog_end -->

</main>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
