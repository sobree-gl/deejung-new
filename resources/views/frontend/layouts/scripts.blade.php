<!-- JS here -->
<script src="{{ asset('plugin_front/frontend/js/vendor/modernizr-3.5.0.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/vendor/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/popper.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/ajax-form.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/waypoints.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/scrollIt.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.scrollUp.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/wow.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/nice-select.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.slicknav.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/plugin_fronts.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/gijgo.min.js') }}"></script>

<!--contact js-->
<script src="{{ asset('plugin_front/frontend/js/contact.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.form.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugin_front/frontend/js/mail-script.js') }}"></script>

<script src="{{ asset('plugin_front/frontend/js/main.js') }}"></script>

<!-- dottext -->
<script>
    $('.dot-ellipsis').css('opacity', '0');
    function dottext() {
        $('.dot-ellipsis').css('opacity', '1');
    }
    setTimeout(dottext, 500);
</script>
<script src="{{ asset('plugin_front/dottext/dottext.js') }}" type="text/javascript"></script>

{{-- Custon js --}}
@if(setting('js.custom_js'))
$( document ).ready(function() {
console.log( "custom_js ready!" );
{!! setting('js.custom_js') !!}
});
@endif
