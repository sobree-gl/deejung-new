<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transection extends Model
{
    protected $fillable = [
        'order_id',
        'agent_id',
        'user_id',
        'title',
        'commistion',
        'withdraw',
        'type',
        'transection_type',
        'state',
        'slip',
        'completed_at',
        'rejected_at',
    ];

    public function order()
    {
        return $this->hasOne( 'App\Orders', 'id', 'order_id' );
    }

    public function user()
    {
        return $this->hasOne( 'App\User', 'id', 'user_id' );
    }

    public function agent()
    {
        return $this->belongsTo( 'App\User' );
    }
}
