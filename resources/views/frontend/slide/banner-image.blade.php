<!--Section: Carousel-->
<section class="">
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            @foreach($banners as $banner)
                @php $images = json_decode($banner->image); @endphp
                @foreach($images as $key => $image)
                    <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                        <img class="d-block w-100" src="{{ Voyager::image($image) }}" alt="First slide">
                    </div>
                @endforeach
            @endforeach
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</section>
<!--Section: Carousel-->
