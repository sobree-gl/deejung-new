<?php

namespace App\Services;

use App\User;
use App\SocialLogin;
use Laravel\Socialite\Contracts\User as ProviderUser;
use Illuminate\Support\Str;
use Cookie;

class SocialAuthService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        //dd($providerUser);
        $account = SocialLogin::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new SocialLogin([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $referred_by = Cookie::get('referral');
                $rendom = Str::random(10);
                $affiliate_id = User::where('affiliate_id', $rendom)->count();
                if ($affiliate_id > 0) {
                    $rendom = Str::random(10);
                } else {
                    if (!empty($referred_by)) {
                        $affiliate = User::where('affiliate_id', $referred_by)->first();
                    }
                    $user = User::create([
                        'email'    => $providerUser->getEmail(),
                        'name'     => $providerUser->getName(),
                        'password' => NULL,
                        'affiliate_id' => $rendom,
                        'referred_by'  => $affiliate->id ?? NULL,
                        'role_id' => 3,
                        'avatar'   => $providerUser->avatar_original ?? config('voyager.user.default_avatar', 'users/default.png')
                    ]);
                }
            }

            $account->user()->associate($user);

            $account->save();

            return $user;
        }
    }
}
