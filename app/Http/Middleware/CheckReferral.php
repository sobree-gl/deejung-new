<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckReferral
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
        if ( $request->hasCookie( 'referral' ) && $request->cookie( 'refCode' ) === $request->query( 'ref' ) ) {
            return $next( $request );
        } else {

            if ( $request->query( 'ref' ) ) {
                $referral = User::where( 'affiliate_id', $request->query( 'ref' ) )->first();

                if ( $referral && $referral->role()->pluck( 'name' )->contains( 'business' ) ) {
                    return redirect( $request->fullUrl() )
                        ->withCookie( cookie()->forever( 'referral', $referral->id ) )
                        ->withCookie( cookie()->forever( 'refCode', $request->query( 'ref' ) ) );
                } else {
                    Cookie::queue( Cookie::forget( 'referral' ) );
                    Cookie::queue( Cookie::forget( 'refCode' ) );
                }
            }
        }

        return $next( $request );
    }
}
