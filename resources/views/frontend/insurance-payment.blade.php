@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@endsection
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@endsection
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@endsection
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
<style>
.numeric {
    background: rgb(255, 255, 255);
}
</style>
@endpush

<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-2">
                        <h3>รายการสั่งซื้อ</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="form-contact" action="{{ route('insurance.store') }}" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อรายการ</th>
                                            <th>ราคา</th>
                                            <th class="text-center">จำนวน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img src="{{ url('/images/2.jpg') }}" alt="" style="width: 130px;"></td>
                                            <td>{{ $tour->title }}</td>
                                            <td>
                                                @if(isset($tour->discount) && (NOW() >= $tour->promotion_start) && (NOW() <= $tour->promotion_end))
                                                    {{ number_format($tour->discount) }} บาท
                                                @else
                                                    {{ number_format($tour->price) }} บาท
                                                @endif
                                            </td>
                                            <td>
                                                <div class="number" style="display: block ruby;">
                                                    <span class="minus">-</span>
                                                    <input type="text" class="numeric" value="1" disabled style="cursor: no-drop;"/>
                                                    <span class="plus">+</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12">
                                <div class="box-shadows">
                                    @if($tour->category_id > 1)
                                    <h5 class="c-o text-center">กรอกรายละเอียดประกันภัย</h5>
                                    <div class="row mt-4">
                                        <div class="form-group col-6">
                                            <select class="form-control" name="insurance">
                                                <option value="" class="" selected="selected">เลือกประกันภัยการเดินทาง</option>
                                                <option value="ประกันภัยการเดินทาง">ประกันภัยการเดินทาง</option>
                                                <option value="สำหรับศึกษาต่อต่างประเทศ">สำหรับศึกษาต่อต่างประเทศ</option>
                                                <option value="Work and Holiday">Work and Holiday</option>
                                                <option value="Sugoi Japan">Sugoi Japan</option>
                                                <option value="Adventurous Asia and Worldwide">Adventurous Asia and Worldwide</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-6">
                                            <select class="form-control" id="subtTyperesource" name="insurance_type">
                                                <option value="" selected="selected">เลือกแผนประกันภัย</option>
                                                <option value="ประกันภัยการเดินทางสำหรับนักท่องเที่ยว (Common Sense,Good Sense)">ประกันภัยการเดินทางสำหรับนักท่องเที่ยว (Common Sense,Good Sense)</option>
                                                <option value="ประกันภัยการเดินทางสำหรับขอวีซ่า เชงเก้น  (Sixth Sense,Sixth Sense ESP)">ประกันภัยการเดินทางสำหรับขอวีซ่า เชงเก้น  (Sixth Sense,Sixth Sense ESP)</option>
                                                <option value="ประกันภัยการเดินทางสำหรับนักธุรกิจ (Business Sense)">ประกันภัยการเดินทางสำหรับนักธุรกิจ (Business Sense)</option>
                                                <option value="ประกันภัยการเดินทางสำหรับนักท่องเที่ยวตัวยง  (Perfect Sense)">ประกันภัยการเดินทางสำหรับนักท่องเที่ยวตัวยง  (Perfect Sense)</option>
                                                <option value="ประกันภัยการเดินทางสำหรับครอบครัว (Common Sense,Good Sense)">ประกันภัยการเดินทางสำหรับครอบครัว (Common Sense,Good Sense)</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-6">
                                            <input type="text" class="form-control" placeholder="กรอกต้นทาง" name="origin" value="{{ old('origin') }}">
                                        </div>

                                        <div class="form-group col-6">
                                            <input type="text" class="form-control" placeholder="กรอกปลายทาง" name="destination" value="{{ old('destination') }}">
                                        </div>

                                        <div class="col-12">
                                            <div class="row" id="input-daterange">
                                                <div class="form-group col-6">
                                                    <input type="text" class="form-control date_range" name="date_start" value="{{ old('date_start') }}">
                                                </div>

                                                <div class="form-group col-6">
                                                    <input type="text" class="form-control date_range" name="date_end" value="{{ old('date_end') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    @endif

                                    <h5 class="c-o text-center">กรอกรายละเอียดผู้ซื้อ{{ $tour->category_id > 1 ? 'ประกันภัย' : 'VISA' }}</h5>

                                    <div class="row mt-4">
                                        <div class="form-group col-6">
                                            <label for="">ชื่อ</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="">นามสกุล</label>
                                            <input type="text" class="form-control" name="surname" value="{{ old('surname') }}">
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="">เบอร์โทร</label>
                                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="">เลขบัตรประชาชน/หนังสือเดินทาง</label>
                                            <input type="text" class="form-control" name="card_id" value="{{ old('card_id') }}">
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="">ที่อยู่</label>
                                            <textarea type="text" class="form-control" name="address"> {{ old('address') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                            @csrf
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="orders[0][tour_id]" value="{{ $tour->id }}">
                            @if(isset($tour->discount) && (NOW() >= $tour->promotion_start) && (NOW() <= $tour->promotion_end))
                                <input type="hidden" id="price" name="orders[0][price]" value="{{ $tour->discount }}">
                                <input type="hidden" id="price_default" value="{{ $tour->discount }}">
                            @else
                                <input type="hidden" id="price" name="orders[0][price]" value="{{ $tour->price }}">
                                <input type="hidden" id="price_default" value="{{ $tour->price }}">
                            @endif
                            <input type="hidden" id="amount" name="orders[0][amount]" value="1">

                            <div class="box-shadows">
                                <div class="text-center">
                                    <h4 class="c-o">สรุปยอดสั่งซื้อ</h4>
                                </div>
                                <div>
                                    <p>ชื่อ : {{ Auth::user()->name }} {{ Auth::user()->last_name }}</p>
                                    <p>อีเมล : {{ Auth::user()->email }}</p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-6">
                                        <p>
                                            @if(isset($tour->discount) && (NOW() >= $tour->promotion_start) && (NOW() <= $tour->promotion_end))
                                                {{ number_format($tour->discount) }}
                                            @else
                                                {{ number_format($tour->price) }}
                                            @endif
                                            x <span id="total">1</span>
                                        </p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p><span id="price_sum">
                                            @if(isset($tour->discount) && (NOW() >= $tour->promotion_start) && (NOW() <= $tour->promotion_end))
                                                {{ number_format($tour->discount) }}
                                            @else
                                                {{ number_format($tour->price) }}
                                            @endif
                                        </span> บาท</p>
                                    </div>
                                    <div class="col-6">
                                        <p>ค่าธรรมเนียน</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p>0 บาท</p>
                                    </div>
                                    <div class="col-6">
                                        <h4><b>รวม</b></h4>
                                    </div>
                                    <div class="col-6 text-right">
                                        <h4 class="c-t">
                                            <strong>
                                                <span id="price_sum">
                                                    @if(isset($tour->discount) && (NOW() >= $tour->promotion_start) && (NOW() <= $tour->promotion_end))
                                                        {{ number_format($tour->discount) }}
                                                    @else
                                                        {{ number_format($tour->price) }}
                                                    @endif
                                                </span> บาท
                                            </strong>
                                        </h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <h4 class="c-o">ช่องทางการรับเงิน</h4>
                                </div>
                                <div class="row">
                                    <div class="col-12 mt-2">
                                        <div class="">
                                            <label class="form-check-label"> โอนเงิน
                                                <input type="radio" class="form-check-input" name="payment_type" value="TRANSFER" checked>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div id="payment1" class="font-14">
                                            <p class="mt-2">ชื่อบัญชี xxxx-xxxxx</p>
                                            <p>เลขที่บัญชี xxxx-xxxxx</p>
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput" style="height: 40px;">
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-append">
                                                    <span class="input-group-text fileinput-exists" data-dismiss="fileinput">
                                                        ลบ
                                                        </span>
                                                    <span class="input-group-text btn-file">
                                                        <span class="fileinput-new">อัปโหลด</span>
                                                        <span class="fileinput-exists">เปลี่ยน</span>
                                                        <input type="file" name="slip" accept="image/*">
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <div class="">
                                            <label class="form-check-label"> บัครเครดิต หรือ paypal
                                                <input type="radio" class="form-check-input" name="payment_type" value="PAYPAL">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div id="payment2" class="font-14" style="display: none;">
                                            <div class="genric-btn primary circle wfull mt-1">
                                                <img src="{{ url('/img/paypal.png') }}" alt="" style="width: 85px;">
                                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <button class="genric-btn danger circle wfull">ยกเลิก</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="submit" class="genric-btn success circle wfull">ยืนยันการสั่งซื้อ</button>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
<script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
<script>
    $('[data-countdown]').each(function () {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function (event) {
            if (event.strftime('%-D') == 0) {
                $this.html(event.strftime('%H:%M:%S'));
            } else {
                $this.html(event.strftime('%-D วัน  %H:%M:%S'));
            }
        });
    });

</script>
<script>
    $(document).ready(function () {
        $('#input-daterange').datepicker({
            inputs: $('.date_range'),
            format: 'dd/mm/yyyy',
            startDate: 'now'
        });


        calculate();
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
        $('.number span').click(function () {
            calculate();
        });
    });
    function calculate(){
        var $input = $('.number input').val();
        var $price = $('input[id="price_default"]').val();
        var $price_sum = $price * $input;
        $('input[id="price"]').val($price_sum);
        $('input[id="amount"]').val($input);
        // alert($input);
        $('span[id="price_sum"]').html(number_format($price_sum));
        $('span[id="total"]').html(number_format($input));
    }
    function number_format(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>
<script>
    $(document).on("input", ".numeric", function() {
        this.value = this.value.replace(/\D/g,'');
    });

    var radioValue = $("input[name='payment_type']:checked").val();
    payment_type(radioValue);
    $("input[name='payment_type']").click(function () {
        payment_type($(this).val());
    });

    function payment_type(nStr){
        if (nStr=='PAYPAL') {
            $("#payment2").css("display", "block");
            $("#payment1").css("display", "none");
        }else{
            $("#payment1").css("display", "block");
            $("#payment2").css("display", "none");
        }
    }
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@endsection
