@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="my-3 text-center">
                <h2>เข้าสู่ระบบ</h2>
                <span>** กรุณากรอกข้อมูลให้ครบถ้วน</span>
            </div>
            <div class="">
                <!-- <div class="card-header">{{ __('กรุณาเข้าสู่ระบบ') }}</div> -->
                <div class="card-body my-3">
                    <form class="form-contact" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

                            <div class="offset-md-2 col-md-8">
                                <input id="email" type="email" placeholder="กรุณาป้อนอีเมล" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> -->

                            <div class="offset-md-2 col-md-8">
                                <input id="password" type="password" placeholder="กรุณาระบุรหัสผ่าน" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="offset-md-2 col-md-8">
                                <button type="submit" class="genric-btn success circle wfull">
                                    {{ __('LOGIN') }}
                                </button>

                                <div class="text-right">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="row">
                        <div class="offset-md-2 col-md-8">
                            <div class="text-center">
                                <a class="genric-btn circle large btn-facrbook" href="{{route('facebook.redirect')}}"><b>FACEBOOK
                                        LOGIN</b></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-md-8">
                            <div class="text-center">
                                <a href="{{route('register')}}">
                                    <button type="submit" class="genric-btn primary circle wfull">Register</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
