@extends('voyager::master')
@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).'
'.$dataType->getTranslatedAttribute('display_name_singular'))
@section('css')
<!-- font -->
<link href="{{ asset('plugin/fileuploader/dist/font/font-fileuploader.css') }}" media="all" rel="stylesheet">
<!-- css -->
<link href="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugin/fileuploader/drag-drop/css/jquery.fileuploader-theme-dragdrop.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .fileuploader-theme-dragdrop .fileuploader-input {
        padding: unset;
    }
</style>
<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css" href="{{ asset('plugin/fancybox/source/jquery.fancybox.css') }}"
    media="screen" />
<style>
    .panel .mce-panel {
        border-left-color: #fff;
        border-right-color: #fff;
    }

    .panel .mce-toolbar,
    .panel .mce-statusbar {
        padding-left: 20px;
    }

    .panel .mce-edit-area,
    .panel .mce-edit-area iframe,
    .panel .mce-edit-area iframe html {
        padding: 0 10px;
        min-height: 350px;
    }

    .mce-content-body {
        color: #555;
        font-size: 14px;
    }

    .panel.is-fullscreen .mce-statusbar {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 200000;
    }

    .panel.is-fullscreen .mce-tinymce {
        height: 100%;
    }

    .panel.is-fullscreen .mce-edit-area,
    .panel.is-fullscreen .mce-edit-area iframe,
    .panel.is-fullscreen .mce-edit-area iframe html {
        height: 100%;
        position: absolute;
        width: 99%;
        overflow-y: scroll;
        overflow-x: hidden;
        min-height: 100%;
    }
</style>
@stop
@section('page_header')
@php
$edit = !is_null($dataTypeContent->getKey());
$add = is_null($dataTypeContent->getKey());
@endphp
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i>
    {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
</h1>
@include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content container-fluid">
    <form role="form" class="form-edit-add"
        action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
        method="POST" enctype="multipart/form-data">
        <!-- PUT Method if we are editing -->
        @if(isset($dataTypeContent->id))
        <input type="hidden" id="edit_" value="{{ $dataTypeContent->getKey() }}">
        {{ method_field("PUT") }}
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-8">
                <!-- ### TITLE ### -->
                <div class="panel">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="voyager-character"></i> {{ __('voyager::post.title') }}
                            <span class="panel-desc"> {{ __('voyager::post.title_sub') }}</span>
                        </h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body" id="title_page">
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'title',
                        '_field_trans' => get_field_translations($dataTypeContent, 'title')
                        ])
                        <input type="text" class="form-control" id="title" name="title"
                            placeholder="{{ __('voyager::generic.title') }}"
                            value="{{ $dataTypeContent->title ?? '' }}">
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="status">{{ 'Gallery Tour' }}</label>
                            <input type="file" name="works" class="gallery_media">
                        </div>
                        <br>
                        <div class="form-group">
                            @if(isset($dataTypeContent->works))
                            <?php $images = json_decode($dataTypeContent->works); ?>
                            @if($images != null)
                            @foreach($images as $image)
                            <div class="img_settings_container" data-field-name="works"
                                style="float:left;padding-right:15px;">
                                <a href="#" class="voyager-x remove-multi-image" style="position: absolute;"></a>
                                <a class="fancybox" rel="group-multi" href="{{ Voyager::image( $image ) }}">
                                    <img src="{{ Voyager::image( $image ) }}" data-file-name="{{ $image }}"
                                        data-id="{{ $dataTypeContent->getKey() }}"
                                        style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
                                </a>
                            </div>
                            @endforeach
                            @endif
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- ### CONTENT ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ __('voyager::post.content') }}</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'body',
                        '_field_trans' => get_field_translations($dataTypeContent, 'body')
                        ])
                        @php
                        $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                        $row = $dataTypeRows->where('field', 'body')->first();
                        @endphp
                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                    </div>
                </div><!-- .panel -->
                <!-- ### EXCERPT ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{!! __('voyager::post.excerpt') !!}</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'excerpt',
                        '_field_trans' => get_field_translations($dataTypeContent, 'excerpt')
                        ])
                        <textarea class="form-control" name="excerpt">{{ $dataTypeContent->excerpt ?? '' }}</textarea>
                    </div>
                </div>
                <!-- ### Tour PRICE DETAILS  ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ 'Price Detail' }}</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <h5 for="status">{{ 'Price' }}</h5>
                            <?php
                    $price_mode = $dataTypeRows->where('field', 'price_mode')->first();
                    ?>
                        </div>
                        <div id="price_mode_amount">
                            <div class="form-group">
                                <label for="status">{{ 'Price' }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'price',
                                '_field_trans' => get_field_translations($dataTypeContent, 'price')
                                ])
                                <input class="form-control" type="number" name="price"
                                    value="{{ $dataTypeContent->price ?? '0' }}">
                            </div>
                            <div class="form-group">
                                <label for="status">{{ 'Discount' }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'discount',
                                '_field_trans' => get_field_translations($dataTypeContent, 'discount')
                                ])
                                <input class="form-control" type="number" name="discount"
                                    value="{{ $dataTypeContent->discount ?? '0' }}">
                            </div>
                            <hr>
                            <div class="panel-heading">
                            </div>
                            <div class="form-group">
                                <label for="status">{{ 'Promotion Start Date' }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'promotion_start',
                                '_field_trans' => get_field_translations($dataTypeContent, 'promotion_start')
                                ])
                                <input type="text" class="form-control datepicker" id="promotion_start"
                                    name="promotion_start" placeholder="promotion start" value="">
                            </div>
                            <div class="form-group">
                                <label for="status">{{ 'Promotion End Date' }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'promotion_end',
                                '_field_trans' => get_field_translations($dataTypeContent, 'promotion_end')
                                ])
                                <input type="text" class="form-control datepicker" id="promotion_end"
                                    name="promotion_end" placeholder="promotion end" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- ### WORKS VIDEO ### -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ 'Video tour' }}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="status">{{ 'add to your video link work' }}</label>
                                    <br>
                                    <code>Video URL (YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)
                            </code>
                                    <?php
                                $works_video = $dataTypeRows->where('field', 'works_video')->first();
                                ?>
                                </div>
                                <div class="form-group">
                                    {!! app('voyager')->formField($works_video, $dataType, $dataTypeContent) !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- ### DETAILS ### -->
                <div class="panel panel panel-bordered panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="slug">{{ __('voyager::post.slug') }}</label>
                            @include('voyager::multilingual.input-hidden', [
                            '_field_name' => 'slug',
                            '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                            ])
                            {{-- <input type="text" class="form-control" id="slug" name="slug"
                                    placeholder="slug"
                                    {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}
                                    value="{{ $dataTypeContent->slug ?? '' }}"> --}}
                            <input type="text" class="form-control" id="slug" name="slug" placeholder="slug" {!!
                                isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug" ) !!}
                                value="{{ $dataTypeContent->slug ?? '' }}" data-db="{{ $dataType->name }}">
                            <label id="slug-error-dup" class="dup-error" for="{{ "slug" }}"
                                style="display: none;color:red">{{ "slug ซ้ำ" }}</label>
                            @push('custom-scripts')
                            @include('javascript.slug-js');
                            @endpush
                        </div>
                        <div class="form-group">
                            <label for="status">{{ __('voyager::post.status') }}</label>
                            <select class="form-control" name="status">
                                <option value="PUBLISHED" @if(isset($dataTypeContent->status) &&
                                    $dataTypeContent->status ==
                                    'PUBLISHED') selected="selected"@endif>{{ __('voyager::post.status_published') }}
                                </option>
                                <option value="DRAFT" @if(isset($dataTypeContent->status) && $dataTypeContent->status ==
                                    'DRAFT')
                                    selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                                <option value="PENDING" @if(isset($dataTypeContent->status) && $dataTypeContent->status
                                    ==
                                    'PENDING') selected="selected"@endif>{{ __('voyager::post.status_pending') }}
                                </option>
                            </select>
                        </div>
                        @php
                        foreach ($dataTypeRows as $key => $data){
                        if($data->field == 'tour_belongsto_category_relationship'){
                        $categories = $data;
                        }
                        }
                        $row = $categories;
                        @endphp
                        <div class="form-group">
                            <label for="category_id">{{ __('voyager::post.category') }}</label>
                            @if ($row->type == 'relationship')
                            @include('voyager::formfields.relationship', ['options' => $row->details , 'required' =>
                            'required'])
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input id="featured" type="checkbox" name="featured"
                                    @if(isset($dataTypeContent->featured) &&
                                $dataTypeContent->featured) checked="checked"@endif>
                                <label for="featured">{{ __('voyager::generic.featured') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ### IMAGE ### -->
                <div class="panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('voyager::post.image') }}</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            @if(isset($dataTypeContent->image))
                            <div data-field-name="image">
                                <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
                                <a class="fancybox" rel="group" href="{{ Voyager::image( $dataTypeContent->image ) }}">
                                    <img src="@if( !filter_var($dataTypeContent->image, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->image ) }}@else{{ $dataTypeContent->image }}@endif"
                                        data-file-name="{{ $dataTypeContent->image }}"
                                        data-id="{{ $dataTypeContent->getKey() }}"
                                        style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                </a>
                            </div>
                            @endif
                            <input type="file" name="image">
                        </div>
                    </div>
                </div>
                <!-- ### SEO CONTENT ### -->
                <div class="panel panel-bordered panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-search"></i> {{ __('voyager::post.seo_content') }}
                        </h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                            @include('voyager::multilingual.input-hidden', [
                            '_field_name' => 'seo_title',
                            '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                            ])
                            <input type="text" class="form-control" name="seo_title" placeholder="SEO Title"
                                value="{{ $dataTypeContent->seo_title ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                            @include('voyager::multilingual.input-hidden', [
                            '_field_name' => 'meta_description',
                            '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                            ])
                            <textarea class="form-control"
                                name="meta_description">{{ $dataTypeContent->meta_description ?? '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                            @include('voyager::multilingual.input-hidden', [
                            '_field_name' => 'meta_keywords',
                            '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                            ])
                            <textarea class="form-control"
                                name="meta_keywords">{{ $dataTypeContent->meta_keywords ?? '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary pull-right">
            @if(isset($dataTypeContent->id)){{ __('voyager::post.update') }}@else <i class="icon wb-plus-circle"></i>
            {{ __('voyager::post.new') }} @endif
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
        enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
        {{ csrf_field() }}
        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
    </form>
</div>
<div class="modal fade modal-danger" id="confirm_delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
            </div>
            <div class="modal-body">
                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                <button type="button" class="btn btn-danger"
                    id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                </button>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<!-- js -->
<script src="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.js') }}" type="text/javascript"></script>
{{-- fancybox --}}
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-buttons.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-media.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    $('document').ready(function () {
            //$('#slug').slugify();
            $(".tag").select2({
                tags: true,
                tokenSeparators: [',']
            })

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
             $(el).slugify();
         });
            $('.form-group').on('click', '.remove-multi-image', function (e) {
             e.preventDefault();
             $image = $(this).siblings('img');
             params = {
                 slug:   '{{ $dataType->slug }}',
                 image:  $image.data('image'),
                 id:     $image.data('id'),
                 field:  $image.parent().data('field-name'),
                 _token: '{{ csrf_token() }}'
             }
             $('.confirm_delete_name').text($image.data('image'));
             $('#confirm_delete_modal').modal('show');
         });
            $('#confirm_delete').on('click', function(){
             $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                 if ( response
                     && response.data
                     && response.data.status
                     && response.data.status == 200 ) {
                    toastr.success(response.data.message);
                $image.parent().fadeOut(300, function() { $(this).remove(); })
            } else {
             toastr.error("Error removing image.");
         }
     });
             $('#confirm_delete_modal').modal('hide');
         });
            $('[data-toggle="tooltip"]').tooltip();
        });
        $(document).ready(function() {
        //fileuploader
            $('input[name="image"]').fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                    '<div class="fileuploader-icon-main"></div>' +
                    '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
                    '<p>${captions.or}</p>' +
                    '<button type="button" class="fileuploader-input-button"><span>${captions.button}</span></button>' +
                    '</div>' +
                '</div>',
            theme: 'dragdrop',
            maxSize: 1,
            fileMaxSize: 3,
            extensions: ['image/*'],
            limit: 1,
            quality:80,
            });
            //fancybox
            $(".fancybox").fancybox();

            //gallery
            $('input.gallery_media').fileuploader({
                limit: 12,
                maxSize: 20,
                addMore: true,
                sorter: {
                selectorExclude: null,
                placeholder: null,
                scrollContainer: window,
                onSort: function(list, listEl, parentEl, newInputEl, inputEl) {
                // onSort callback
                }
                }
            });
        });

</script>
@stack('custom-scripts')
@stop
