<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use App\Banner;
use App\Post;
use App\Tour;
use App\Page;
use App\Promotion;

class MainController extends Controller
{
    public function index()
    {
        //get banner home
        $banners = Banner::page('home')->get();
        //get service
        $services = Service::published()->where('featured', 1)
            ->offset(0)
            ->limit(6)
            ->order()
            ->get();

        //get post
        $posts = Post::published()
            ->publicat()
            ->where('featured', 1)
            ->offset(0)
            ->limit(3)
            ->order()
            ->get();


        //get Customer
        $tours_categories = Tour::order()
            ->with([
                'categorytour'
            ])
            ->whereNotIn('category_id', [1, 2])
            ->get()
            ->groupBy('categorytour.name');

        $promotions = Promotion::published()
            ->order()
            ->get();

        return view('frontend.home', compact('services', 'banners', 'posts', 'tours_categories', 'promotions'));
    }

    public function show(Request $request, $slug)
    {
    }
}
