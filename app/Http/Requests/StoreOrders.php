<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOrders extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = [
            'payment_type' => ['required', Rule::in( ['TRANSFER', 'PAYPAL'] )],
            'slip'         => 'required_if:payment_type,TRANSFER|nullable|image',
        ];

        foreach ( $this->request->get( 'orders' ) as $key => $order ) {
            foreach ( $order as $keyItem => $val ) {
                $roles['orders.' . $key . '.' . $keyItem] = 'required|numeric|exists:App\Tour,id'; // check Tour id
                $roles['orders.' . $key . '.' . $keyItem] = 'required|numeric|min:1'; // check price
                $roles['orders.' . $key . '.' . $keyItem] = 'required|numeric|min:1'; // check amount

            }
        }

        return $roles;
    }
}
