<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TransectionController as Transection;
use App\Transection as MoneyModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function income(Request $request)
    {
        if (!auth()->user()->role()->pluck('name')->contains('business')) {
            return abort(403);
        }
        $transections = Transection::index($request);
        $money = Transection::money();
        $withdraw = Transection::withdraw();

        return view('frontend.member-income', ['transections' => $transections, 'money' => $money, 'withdraw' => $withdraw]);
    }

    public function withdraw(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'money' => 'required|numeric|min:1',
        ]);

        $validator->after(function ($validator) use ($request) {
            $money = Transection::money();
            if ($request->money > $money) {
                $validator->errors()->add('money', 'จำนวนเงินที่กรอกมากว่าจำนวนเงินที่มีอยู่ในระบบ');
            }

            $withdraw = Transection::withdraw();

            if ($withdraw) {
                $validator->errors()->add('money', 'ท่านมีรายการถอนเงินอยู่ในระหว่างดำเนินการ');

            }
        });

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        Transection::store('', 'WITHDRAW', auth()->user()->id, $request->money);

        return back()->with('status', 'ส่งเรื่องเรียบร้อยแล้ว');
    }

    public function withdrawUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'money' => 'required|numeric|min:1',
        ]);

        $validator->after(function ($validator) use ($request) {
            $money = Transection::money();
            if ($request->money > $money) {
                $validator->errors()->add('money', 'จำนวนเงินที่กรอกมากว่าจำนวนเงินที่มีอยู่ในระบบ');
            }
        });

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $transection = MoneyModel::where('state', 'PROCEED')->where('id', $id)->firstOrFail();

        $transection->withdraw = $request->money;

        $transection->save();

        return back()->with('status', 'บันทึกจำนวนใหม่เรียบร้อย');
    }

}
