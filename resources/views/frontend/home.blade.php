@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
    @include('frontend.layouts.seo')
@stop

@section('navbar')
    @include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
    {{-- @include('frontend.slide.banner-video')  --}}
    @isset($banners)
        @include('frontend.slide.banner-image')
    @endisset
@stop
@section('content')
    <main class="">
        <!-- popular_courses_start -->
        <div class="popular_courses">
            {{--<div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section_title text-center mb-4">
                            <h3>แพ็กเก็จทัวร์</h3>
                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="all_courses">
                <div class="container">
                    @foreach ($tours_categories as $key => $tours)
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="section_title text-left mb-4">
                                    <h4>{{ $key }}</h4>
                                </div>
                            </div>
                        </div>
                        @foreach ($tours as $i => $item)
                            @if($i ==0)
                                <div class="row">
                                    <div class="col-xl-12" style="height: 800px">
                                        <div class="section_title text-left mb-4">
                                            <img class="img-fluid"
                                                 src="{{ Voyager::image($item->categorytour->hero_image) }}">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="row">
                            @foreach ($tours as $item)
                                <div class="col-xl-3 col-lg-3 col-md-4">
                                    <div class="single_courses">
                                        <div class="thumb">
                                            <a href="{{ route('tours.show',$item->slug) }}">
                                                <img src="{{ Voyager::image($item->thumbnail('medium')) }}"
                                                     alt="{{$item->title}}">
                                            </a>
                                        </div>
                                        <div class="courses_info">
                                            <div class="dotellipsis title">
                                                <h6 class="title-crad-display"><a
                                                        href="{{ route('tours.show',$item->slug) }}">{{ $item->title }}</a>
                                                </h6>
                                            </div>
                                            <div class="dotellipsis excerpt">
                                                <span>{{ $item->excerpt }}</span>
                                            </div>
                                            <div class="star_prise d-flex justify-content-between">
                                                @if(isset($item->discount) && (NOW() >= $item->promotion_start) && (NOW() <=
                                                       $item->promotion_end))
                                                    <div class="star promotion">

                                                        <span>โปรโมชั่น</span>
                                                        <div class="countdown"
                                                             data-countdown="{{ $item->promotion_end }}"></div>

                                                    </div>
                                                @endif
                                                <div class="prise">
                                                    @if(isset($item->discount) && (NOW() >= $item->promotion_start) && (NOW() <=
                                                        $item->promotion_end))
                                                        <span
                                                            class="offer">{{ number_format($item->price,0) }} บาท</span>
                                                        <p class="active_prise">
                                                            {{ number_format($item->discount,0) }} บาท
                                                        </p>
                                                    @else
                                                        <p class="active_prise">
                                                            {{ $item->price ? number_format($item->price,0) . 'บาท' : 'ยังไม่กำหนดราคา' }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row text-center">
                                                <div class="col-8">
                                                    <a href="{{ route('tours.show',$item->slug) }}"
                                                       class="genric-btn primary circle wfull">
                                                        รายละเอียด
                                                    </a>
                                                </div>
                                                <div class="col-4">
                                                    @guest
                                                        <a href="{{ route('login') }}">
                                                            @else
                                                                <a href="{{ route('tours.payment',$item->slug) }}">
                                                                    @endguest
                                                                    <img src="{{ url('/img/cart.png') }}" alt=""
                                                                         style="width: 40px;">
                                                                </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    <div class="col-xl-12">
                        <div class="more_courses text-center">
                            <a href="{{ route('tours.index') }}" class="genric-btn info-border circle">ดูทั้งหมด
                                <i
                                    class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- popular_courses_end-->
        <!-- our_latest_blog_start -->
        @if(sizeof($promotions) > 0)
            <div class="our_latest_blog">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="section_title text-center mb-4">
                                <h3>โปรโมชั่นลดหนักมาก</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        @foreach ($promotions as $promotion)
                            <div class="col-xl-4 col-md-4">
                                <div class="single_latest_blog">
                                    <div class="thumb">
                                        <img src="{{ Voyager::image($promotion->thumbnail('medium')) }}" alt="">
                                    </div>
                                    <div class="content_blog">
                                        <div class="date">
                                            <p>12 Jun, 2019 in <a href="#">Design tips</a></p>
                                        </div>
                                        <div class="blog_meta">
                                            <h3><a href="#">{{ $promotion->title }}</a></h3>
                                        </div>
                                        <div class="dot-ellipsis" style="height: 55px;color: rgb(113, 113, 113);">
                                            {!! $promotion->detail !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
    @endif
    <!-- our_latest_blog_end -->
        <!-- subscribe_newsletter_Start -->

        <!-- subscribe_newsletter_end -->
    </main>
@stop
@push('custom-scripts')
    <script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
    <script>
        $('[data-countdown]').each(function () {
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function (event) {
                if (event.strftime('%-D') == 0) {
                    $this.html(event.strftime('%H:%M:%S'));
                } else {
                    $this.html(event.strftime('%-D วัน  %H:%M:%S'));
                }
            });
        });
    </script>
@endpush
@section('footer')
    @include('frontend.layouts.footer')
@stop
