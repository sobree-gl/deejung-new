<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $table = 'insurance';

    protected $fillable = [
        'order_id',
        'insurance',
        'insurance_type',
        'origin',
        'destination',
        'date_start',
        'date_end',
        'name',
        'surname',
        'phone',
        'card_id',
        'address',
    ];
}
