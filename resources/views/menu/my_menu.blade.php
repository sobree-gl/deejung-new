<ul id="navigation">
    @php
    if (Voyager::translatable($items)) {
    $items = $items->load('translations');
    }
    @endphp
    @foreach ($items as $menu_item)
    @php
    $originalItem = $menu_item;
    if (Voyager::translatable($menu_item)) {
    $menu_item = $menu_item->translate($options->locale);
    }
    $isActive = null;
    $styles = null;
    $icon = null;
    // Background Color or Color
    if (isset($options->color) && $options->color == true) {
    $styles = 'color:'.$menu_item->color;
    }
    if (isset($options->background) && $options->background == true) {
    $styles = 'background-color:'.$menu_item->color;
    }
    // Check if link is current
    if(url($menu_item->link()) == url()->current()){
    $isActive = 'active';
    }
    if(url($menu_item->link()) == url('/').'/'.request()->segment(1)){
    $isActive = 'active';
    }
    // Set Icon
    if(isset($options->icon) && $options->icon == true){
    $icon = '<i class="' . $menu_item->icon_class . '"></i>';
    }
    @endphp
    @if(isset($isActive))
    <li><a class="{{$isActive}}" href="{{ $menu_item->link() }}">{{ $menu_item->title }}</a></li>
    @else
    <li><a class="" href="{{ $menu_item->link() }}">{{ $menu_item->title }}</a></li>
    @endif
    @if(!$originalItem->children->isEmpty())
    @endif
    @endforeach

    @guest
    <li>
        <a href="{{ route('login') }}">
        <!-- <a href="#test-form" class="login popup-with-form"> -->
            <i class="flaticon-user"></i>
            <span>เข้าสู่ระบบ</span>
        </a>
    </li>
    @else
    <li>
        <a href="#">
            <!-- <img src="" alt="" style="width: 30px;">&nbsp; -->
            <img src="{{ asset('img/user.png') }}" alt="" style="width: 30px;">
            {{ Auth::user()->name }}
            <i class="ti-angle-down"></i>
        </a>
        <ul class="submenu">
            <li>
                <a href="{{ url('profile') }}">
                    <i class="ti-user"></i>
                    <span>โปรไฟล์ส่วนตัว</span></a>
            </li>
            @if(Auth::user()->role()->pluck( 'name' )->contains( 'business' ))
            <li>
                <a href="{{ route('member.income') }}">
                    <i class="ti-money"></i>
                    <span>รายได้ของคุณ</span></a>
            </li>
            @endif
            <li>
                <a href="{{ route('member.order') }}">
                    <i class="ti-shopping-cart"></i>
                    <span>ประวัติการสั่งซื้อ</span></a>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    <i class="ti-share-alt"></i>&nbsp;<span>ออกจากระบบ</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </li>
    @endguest
</ul>
