@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.css') }}">
@endpush
<main class="">
    <div class="courses_details_info">
        <div class="container">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                <div class="col-12 mb-3 text-center">
                    <h2>รายได้ของคุณทั้งหมด</h2>
                </div>
                @if(empty($withdraw))
                <a href="javascript:;" data-toggle="modal" data-target="#myModal"
                    class="genric-btn info circle mb-3"><b>ขอถอนเงิน</b>
                </a>
                @else
                <div class="col-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>วันที่/เวลา</th>
                                <th>จำนวนเงินที่ถอน</th>
                                <th class="text-right">สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DateThai($withdraw->created_at) }}</td>
                                <td>{{ number_format($withdraw->withdraw, 2) }} บาท</td>
                                <td class="text-right">
                                    <a href="javascript:;" data-toggle="modal" data-target="#edit">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    <span class="badge badge-info">{{ $withdraw->state }}</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
            <div class="row">
                <div class="col-12 my-3 text-center">
                    <h2>ประวัติทางการเงิน</h2>
                </div>
                <div class="col-12 mb-3 text-center">
                    <div class="text-right">
                        <p>
                            จำนวนเงินที่อยู่ในระบบ :
                            <span class="badge badge-info">
                                <b style="font-size: 18px;">{{ number_format($money, 2) }} บาท</b>
                            </span>
                        </p>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>วันที่/เวลา</th>
                                <th>รายการ</th>
                                <th>จำนวนเงิน</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($transections as $transection)
                            <tr>
                                <td>{{ ($transections->currentPage() - 1) * $transections->perPage() + $loop->iteration }}</td>
                                <td>{{ DateThai($transection->created_at) }}</td>
                                <td>
                                    {{ $transection->title }}
                                    @if($transection->transection_type	=== 'TOUR')
                                        จาก {{ $transection->order->details[0]->tour->title }}
                                    @elseif($transection->transection_type	=== 'REGISTER')
                                        จาก {{ $transection->user->name }}
                                    @endif
                                </td>
                                @if($transection->type === 'COMMISTION')
                                    <td style="color: #28a745;">{{ number_format($transection->commistion, 2) }} บาท</td>
                                @else
                                    <td>
                                        <a href="">
                                            <i class="fa fa-file-text"></i>
                                        </a>
                                        <span class="c-r"> -{{ number_format($transection->withdraw, 2) }} บาท</span>
                                    </td>
                                @endif
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="4" class="text-center">ยังไม่มีข้อมูล</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                @if($transections->hasPages())
                <div class="col-12 d-flex justify-content-center">
                    {{ $transections->links() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</main>

<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">รายละเอียดการถอนเงิน</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <form class="form-contact" action="{{ route('member.withdraw') }}" method="post" enctype="multipart/form-data" id="FormProfile">
                <div class="modal-body">
                    <!-- CSRF TOKEN -->
                    @csrf
                    <span>** จำนวนเงินที่ถอนได้ {{ number_format($money, 2) }} บาท</span>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">จำนวนเงินที่ต้องการถอน</label>
                                <input
                                    type="text"
                                    name="money"
                                    class="form-control"
                                    placeholder="กรอกจำนวนเงินที่ต้องการถอน.."
                                    required
                                >
                            </div>
                        </div>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="genric-btn success">ยืนยันการถอนเงิน</button>
                </div>
            </form>
        </div>
    </div>
</div>

@if(!empty($withdraw))
<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">แก้ไขจำนวนเงิน</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <form class="form-contact" action="{{ route('member.withdraw.update', $withdraw->id) }}" method="post" enctype="multipart/form-data" id="FormProfile">
                <div class="modal-body">
                    <!-- CSRF TOKEN -->
                    @method('PUT')
                    @csrf
                    <span>** จำนวนเงินที่ถอนได้ {{ number_format($money, 2) }} บาท</span>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">จำนวนเงินที่ต้องการถอน</label>
                                <input
                                    type="text"
                                    name="money"
                                    class="form-control"
                                    placeholder="กรอกจำนวนเงินที่ต้องการถอน.."
                                    required
                                    value="{{ intval($withdraw->withdraw) }}"
                                >
                            </div>
                        </div>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="genric-btn success">ยืนยันการถอนเงิน</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@stop
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.btn-clipboard');

    clipboard.on('success', function (e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
        alert("Coppied!")
        e.clearSelection();
    });
    clipboard.on('error', function (e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
